package jp.alhinc.shotaro_maruyama.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
/*
author:丸山翔太郎
*/
public class CalculateSales {
	public static void readFile(
		String filePath,
		HashMap<String,String>branchLst,
		HashMap<String,Long>branchSales,
		String fileName,
		String line){

		File file = null; //支店定義ファイルの読み込み
		BufferedReader br = null; //finallyで使うためにtryの外で箱を作る

		try {
			file = new File(filePath,fileName);

			if(file.exists() != true) {
				System.out.println("支店定義ファイル" + fileName + "が存在しません。");
				return;
			}

			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr); //BufferedReader=>１行ずつ読み込むreadlineメソッドがある

			while((line = br.readLine()) != null){ //ファイルの中身がなくなるまで読み込む

				String branchLstKey[] = line.split(",");

				//★エラー処理支店定義ファイルの中確認
				if(( ! branchLstKey[0].matches ("^[0-9]{3}$")) || (branchLstKey.length != 2)){
					System.out.println("支店定義ファイル" + fileName +"のフォーマットが不正です。");
					return;
				}

				branchLst.put(branchLstKey[0] , branchLstKey[1]); //ここの引数に lineの中身を入れて定義する
				branchSales.put(branchLstKey[0] , 0L);
			}

		}catch(IOException e){
			System.out.println("予期せぬエラーが発生しました。プログラムを終了します。");
			return;

		}finally{
			if(br != null){
				try{
					br.close();//closeメソッドに問題があれば↓の処理

				}catch(IOException e) {
					System.out.println("close出来ませんでした。");
				}
			}
		}
	}

	public static void createFile(
		String filePath,
		String fileName,
		HashMap<String, String>branchLst,
		HashMap<String, Long>branchSales) {

		BufferedWriter totalBranchSales = null;
		try {

		File brankFile = new File(filePath , fileName);
		FileWriter fw = new FileWriter(brankFile);
		totalBranchSales = new BufferedWriter(fw);

			for (String key : branchSales.keySet()){
				totalBranchSales.write(key + "," + branchLst.get(key) + "," + branchSales.get(key));
				totalBranchSales.newLine();
			}

		} catch (IOException e) {

			System.out.println("エラーが発生しました。");
			return;

		}finally{
			if(totalBranchSales != null){
				try {
					totalBranchSales.close();

				}catch(IOException e) {
						System.out.println("close出来ませんでした。");
				}
			}
		}
	}



	public static void main(String[] args){
		// ★コマンドライン引数の確認
		if(args.length != 1){

			System.out.println("予期せぬエラーが発生しました。");
			return;
		}

		HashMap<String, String>branchLst = new HashMap<String, String>();
		HashMap<String, Long>branchSales = new HashMap<String, Long>();

		String line =  "";
		 line = "";

		readFile(args[0],branchLst,branchSales,"branch.lst",line);

		File dir = new File(args[0]); //全ファイル抽出
		File[] list = dir.listFiles();

		List<File>salesFiles = new ArrayList<File>();

		for(int i = 0; i < list.length; i ++){
			String files = list[i].getName();

			if(list[i].isFile() && files.matches("^[0-9]{8}.rcd")){
				salesFiles.add(list[i]);
			}
		}

		//★エラー処理ファイルの連番チェック
		Collections.sort(salesFiles);
		for(int i = 0; i < salesFiles.size() - 1; i ++){
			int fileNum = Integer.parseInt(salesFiles.get(i).getName().substring(0,8));
			int nextFile = Integer.parseInt(salesFiles.get(i+1).getName().substring(0,8));

			if(nextFile - fileNum != 1){
				System.out.println("売上ファイルが連番ではありません。");
				return;
			}
		}
		 //読み込みと集計
		 BufferedReader br2 = null;
		 long totalSales;

		 for(int i = 0; i < salesFiles.size(); i++){
			ArrayList<String>fileConts = new ArrayList<>();

			try {
				FileReader fr = new FileReader(salesFiles.get(i)); //salesFilesリストのi番目のファイルをfrに格納
				br2 = new BufferedReader(fr); //bufferedReaderでi番目のファイルの内容をbr2に格納

				while((line = br2.readLine()) != null){ //fileContsの内容を最終行まで１行ずつ読む
					fileConts.add(line);
				}

				String salesFileName = salesFiles.get(i).getName();

				//★エラー処理　売上ファイルの内容が正しいか
				if(fileConts.size() != 2){
					System.out.println(salesFileName + "のフォーマットが不正です。");
					return;
				}

				//★エラー処理売上ファイル内の支店コードの定義有無確認
				if( ! branchLst.containsKey(fileConts.get(0))){
					System.out.println(salesFileName + "の支店コードが不正です。");
					return;
				}

				//★エラー処理　売上業に数字のみか確認
				if( ! fileConts.get(1).matches("^[0-9]+$")){
					System.out.println("予期せぬエラーが発生しました。");
					return;
				}

				String branchCode = fileConts.get(0);
				long salesBuff = Long.parseLong(fileConts.get(1));

				totalSales = salesBuff + (branchSales.get(branchCode));

				branchSales.put(branchCode , totalSales);

				//★エラー処理　合計額上限10桁チェック
				if(totalSales >= 10000000000L){
					System.out.println("合計金額が10桁を超えました");
					return;
				}

			} catch(IOException e) {
				System.out.println("エラーが発生しました。");
				return;

			}finally{
				if(br2 != null){

					try {
						br2.close();//closeメソッドに問題があれば↓の処理

					}catch(IOException e){
						System.out.println("close出来ませんでした。");
					}
				}
			}
			createFile(args[0],"branch.out",branchLst,branchSales);
		}
	}
}


